package SistemaNomina;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Empleados extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private ButtonGroup agrupadorRol;// Agrupador para los RadioButton
	private ButtonGroup agrupadorTipo;

	private JRadioButton rdbtnChofer;
	private JRadioButton rdbtnCargador;
	private JRadioButton rdbtnAuxiliar;
	private JRadioButton rdbtnInterno;
	private JRadioButton rdbtnExterno;
	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JLabel lblNmero;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblTipo;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JScrollPane scrollPane_3;
	private JButton btnOk;
	private JButton btnCancel;
	private JScrollPane scrollPane;


	public static int count;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Empleados frame = new Empleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Empleados() {
		setTitle("Empleados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 474, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.setEnabled(false);
		btnNuevo.setBounds(27, 22, 70, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				BuscarEmpleados Empleados = new BuscarEmpleados();
				Empleados.setVisible(true);
				dispose();
			}
		});
		btnBuscar.setBounds(96, 22, 83, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modificar_empleados Empleados = new Modificar_empleados();
				Empleados.setVisible(true);
				dispose();

			}
		});
		btnModificar.setBounds(178, 22, 98, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarEmpleados Empleados = new EliminarEmpleados();
				Empleados.setVisible(true);
				dispose();

			}
		});
		btnEliminar.setBounds(275, 22, 89, 23);
		contentPane.add(btnEliminar);

		lblNmero = new JLabel("N\u00FAmero:");
		lblNmero.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmero.setBounds(27, 72, 63, 39);
		contentPane.add(lblNmero);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					arg0.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField.setBounds(78, 82, 101, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(27, 102, 63, 39);
		contentPane.add(lblNombre);

		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				char c = arg0.getKeyChar();
				if (textField_1.getText().length() >= 50)
					arg0.consume();
				if (Character.isDigit(c)) {
					getToolkit().beep();
					arg0.consume();
					JOptionPane.showMessageDialog(null, "Ingresar solo letras",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		textField_1.setBounds(78, 113, 319, 28);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(48, 157, 63, 39);
		contentPane.add(lblRol);

		rdbtnChofer = new JRadioButton("Chofer");
		rdbtnChofer.setBounds(78, 166, 109, 23);
		contentPane.add(rdbtnChofer);

		rdbtnCargador = new JRadioButton("Cargador");
		rdbtnCargador.setBounds(78, 192, 109, 23);
		contentPane.add(rdbtnCargador);

		rdbtnAuxiliar = new JRadioButton("Auxiliar");
		rdbtnAuxiliar.setBounds(78, 218, 109, 23);
		contentPane.add(rdbtnAuxiliar);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(232, 157, 63, 39);
		contentPane.add(lblTipo);

		rdbtnInterno = new JRadioButton("Interno");
		rdbtnInterno.setBounds(268, 166, 109, 23);
		contentPane.add(rdbtnInterno);

		rdbtnExterno = new JRadioButton("Externo");
		rdbtnExterno.setBounds(268, 192, 109, 23);
		contentPane.add(rdbtnExterno);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 146, 166, 112);
		contentPane.add(scrollPane_1);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(213, 146, 182, 112);
		contentPane.add(scrollPane_2);

		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 72, 428, 200);
		contentPane.add(scrollPane_3);

		agrupador();

		btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numemp,nombre;
				int choferAlm,choferCarg,choferAux,tipoInt,tipoExt,longNumEmp,longNombre;

				numemp = "";
				nombre = "";
				choferAlm = 0;
				choferCarg = 0;
				choferAux = 0;
				tipoInt = 0;
				tipoExt = 0;
				count = 0;
				longNumEmp = 0;
				longNombre = 0;

				numemp = textField.getText();
				nombre = textField_1.getText();

				longNumEmp = numemp.length();
				longNombre = nombre.length();

				if (rdbtnChofer.isSelected() == (true)) {
					choferAlm = 1;
				} else if (rdbtnCargador.isSelected() == (true)) {
					choferCarg = 1;
				} else if (rdbtnAuxiliar.isSelected() == (true)) {
					choferAux = 1;
				}

				if (rdbtnInterno.isSelected() == (true)) {
					tipoInt = 1;
				} else if (rdbtnExterno.isSelected() == (true)) {
					tipoExt = 1;
				}

				if (longNumEmp == 0) {
					JOptionPane.showMessageDialog(null,
							"Ingresar Numero de empleado");
				} else if (longNombre == 0) {
					JOptionPane.showMessageDialog(null, "Ingresar Nombre");
				} else if (choferAlm == 0 && choferCarg == 0 && choferAux == 0) {
					JOptionPane.showMessageDialog(null, "Seleccionar Rol");
				} else if (tipoInt == 0 && tipoExt == 00) {
					JOptionPane.showMessageDialog(null, "Seleccionar Tipo");
				} else if (count > 1) {
					JOptionPane.showMessageDialog(null,
							"Empleado ya registrado");
				} else {
					// Manda llamar el metodo para insertar los valores del
					// empleado.
					existeEmp(numemp);
					if (count > 1) {
						JOptionPane.showMessageDialog(null,
								"Empleado ya registrado");
					} else {
						// Inserta en la tabla y despues limpia los datos del
						// formulario
						insertempelados(numemp, nombre, choferAlm, choferCarg,
								choferAux, tipoInt, tipoExt);
						limpiar();
					}
				}
			}
		});
		btnOk.setBounds(250, 297, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(349, 297, 89, 23);
		contentPane.add(btnCancel);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 428, 50);
		contentPane.add(scrollPane);
	}

	private void insertempelados(String numemp2, String nombre2, int choferAlm,
			int choferCarg, int choferAux, int tipoInt, int tipoExt) {
		// TODO Auto-generated method stub
		try {
			MyDataAcces conexion = new MyDataAcces();
			conexion.setQuery("INSERT INTO `empleados`(`Numero`, `Nombre`,`Chofer`,`Cargador`,`Auxiliar`,`tipoInt`,`tipoExt`) VALUES ('"
					+ numemp2
					+ "','"
					+ nombre2
					+ "','"
					+ choferAlm
					+ "','"
					+ choferCarg
					+ "','"
					+ choferAux
					+ "','"
					+ tipoInt
					+ "','"
					+ tipoExt + "')");
			JOptionPane.showMessageDialog(null,
					"Los datos se guardaron correctamente");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error : Al guardar los datos del empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	private void existeEmp(String numemp) {
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;

		try {
			resultado = conexion
					.getQuery("select * from empleados where Numero=" + numemp
							+ "");
			while (resultado.next()) {
				count++;
				// System.out.println(count);
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	public void limpiar() {

		textField.setText("");
		textField_1.setText("");

		agrupadorRol.remove(rdbtnChofer);
		agrupadorRol.remove(rdbtnCargador);
		agrupadorRol.remove(rdbtnAuxiliar);

		agrupadorTipo.remove(rdbtnExterno);
		agrupadorTipo.remove(rdbtnInterno);

		rdbtnChofer.setSelected(false);
		rdbtnCargador.setSelected(false);
		rdbtnAuxiliar.setSelected(false);
		rdbtnInterno.setSelected(false);
		rdbtnExterno.setSelected(false);

		agrupador();

	}
	public void agrupador() {
		
		agrupadorRol = new ButtonGroup();
		agrupadorRol.add(rdbtnChofer);
		agrupadorRol.add(rdbtnCargador);
		agrupadorRol.add(rdbtnAuxiliar);

		agrupadorTipo = new ButtonGroup();
		agrupadorTipo.add(rdbtnInterno);
		agrupadorTipo.add(rdbtnExterno);
	}

}
