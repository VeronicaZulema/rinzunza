package SistemaNomina;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuPrincipal extends JFrame {
	private JPanel contentPane;
	private JLabel lblMenuPrincipal;
	private JButton btnAgregarEmpleado;
	private JButton btnBuscarEmpleado;
	private JButton btnModificarEmpleado;
	private JButton btnEliminarEmpleado;
	private JButton btnSalir;
	private JButton btnCapturaDeMovimientos;
	private JButton btnCapturarMovimientos;
	private JButton btnBuscarMovimientos;
	private JButton btnModificarMovimientos;
	private JButton btnEliminarMovimientos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					javax.swing.UIManager.put("Button.background", Color.blue);
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(MenuPrincipal.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(MenuPrincipal.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(MenuPrincipal.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MenuPrincipal.class.getName())
					.log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {
				new MenuPrincipal().setVisible(true);
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		lblMenuPrincipal = new JLabel("Menu Principal");
		lblMenuPrincipal.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMenuPrincipal.setBounds(160, 23, 168, 27);
		contentPane.add(lblMenuPrincipal);

		btnAgregarEmpleado = new JButton("Agregar Empleado");
		btnAgregarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Empleados MenuPrincipal = new Empleados();
				MenuPrincipal.setVisible(true);
			}
		});
		btnAgregarEmpleado.setBounds(36, 71, 146, 33);
		contentPane.add(btnAgregarEmpleado);

		btnBuscarEmpleado = new JButton("Buscar Empleado");
		btnBuscarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarEmpleados MenuPrincipal = new BuscarEmpleados();
				MenuPrincipal.setVisible(true);

			}
		});
		btnBuscarEmpleado.setBounds(36, 115, 146, 33);
		contentPane.add(btnBuscarEmpleado);

		btnModificarEmpleado = new JButton("Modificar Empleado");
		btnModificarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modificar_empleados MenuPrincipal = new Modificar_empleados();
				MenuPrincipal.setVisible(true);

			}
		});
		btnModificarEmpleado.setBounds(36, 159, 146, 33);
		contentPane.add(btnModificarEmpleado);

		btnEliminarEmpleado = new JButton("Eliminar Empleado");
		btnEliminarEmpleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarEmpleados MenuPrincipal = new EliminarEmpleados();
				MenuPrincipal.setVisible(true);

			}
		});
		btnEliminarEmpleado.setBounds(36, 203, 146, 33);
		contentPane.add(btnEliminarEmpleado);

		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(375, 292, 89, 27);
		contentPane.add(btnSalir);

		btnCapturaDeMovimientos = new JButton("Consultar Nomina");
		btnCapturaDeMovimientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NominaEmpleados MenuPrincipal = new NominaEmpleados();
				MenuPrincipal.setVisible(true);

			}
		});
		btnCapturaDeMovimientos.setBounds(153, 266, 175, 34);
		contentPane.add(btnCapturaDeMovimientos);

		btnCapturarMovimientos = new JButton("Capturar Movimientos");
		btnCapturarMovimientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CapturaMovimientos MenuPrincipal = new CapturaMovimientos();
				MenuPrincipal.setVisible(true);
			}
		});
		btnCapturarMovimientos.setBounds(281, 71, 146, 33);
		contentPane.add(btnCapturarMovimientos);

		btnBuscarMovimientos = new JButton("Buscar Movimientos");
		btnBuscarMovimientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarMovimientos MenuPrincipal = new BuscarMovimientos();
				MenuPrincipal.setVisible(true);
			}
		});
		btnBuscarMovimientos.setBounds(281, 115, 146, 33);
		contentPane.add(btnBuscarMovimientos);

		btnModificarMovimientos = new JButton("Modificar Movimientos");
		btnModificarMovimientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarMovimientos MenuPrincipal = new ModificarMovimientos();
				MenuPrincipal.setVisible(true);
			}
		});
		btnModificarMovimientos.setBounds(281, 159, 146, 33);
		contentPane.add(btnModificarMovimientos);

		btnEliminarMovimientos = new JButton("Eliminar Movimientos");
		btnEliminarMovimientos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarMovimiento MenuPrincipal = new EliminarMovimiento();
				MenuPrincipal.setVisible(true);
			}
		});
		btnEliminarMovimientos.setBounds(281, 203, 146, 33);
		contentPane.add(btnEliminarMovimientos);
	}
}
