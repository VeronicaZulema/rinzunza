package SistemaNomina;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CapturaMovimientos extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JScrollPane scrollPane;

	private JLabel lblNmeroDeEmpleado;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblFecha;
	private JLabel lblTipo;

	private JCheckBox chckbxCantidadDeEntregas;
	private JCheckBox chckbxCubriTurno;
	private JComboBox comboBox;

	private JSpinner spinner;

	private JButton btnOk;
	private JButton btnCancel;

	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contAce;
	public static int contFech;
	public static String fechasistema;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CapturaMovimientos frame = new CapturaMovimientos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CapturaMovimientos() {
		setTitle("Captura de Movimientos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 584, 399);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		inicializarvar();

		btnNuevo = new JButton("Nuevo");
		btnNuevo.setEnabled(false);
		btnNuevo.setBounds(27, 24, 83, 23);
		contentPane.add(btnNuevo);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarMovimientos CapturaMovimientos = new BuscarMovimientos();
				CapturaMovimientos.setVisible(true);
				dispose();
			}
		});
		btnBuscar.setBounds(112, 24, 89, 23);
		contentPane.add(btnBuscar);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModificarMovimientos CapturaMovimientos = new ModificarMovimientos();
				CapturaMovimientos.setVisible(true);
				dispose();
			}
		});
		btnModificar.setBounds(204, 24, 89, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EliminarMovimiento CapturaMovimientos = new EliminarMovimiento();
				CapturaMovimientos.setVisible(true);
				dispose();

			}
		});
		btnEliminar.setBounds(295, 24, 83, 23);
		contentPane.add(btnEliminar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 531, 50);
		contentPane.add(scrollPane);

		lblNmeroDeEmpleado = new JLabel("N\u00FAmero de empleado:");
		lblNmeroDeEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNmeroDeEmpleado.setBounds(285, 72, 132, 50);
		contentPane.add(lblNmeroDeEmpleado);

		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNombre.setBounds(52, 121, 50, 50);
		contentPane.add(lblNombre);

		lblRol = new JLabel("Rol:");
		lblRol.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblRol.setBounds(52, 158, 42, 56);
		contentPane.add(lblRol);

		lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFecha.setBounds(52, 201, 47, 50);
		contentPane.add(lblFecha);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) { //Evento para cuando se le da clic a la tecla enter.
				String numEmp;

				numEmp = "";

				numEmp = textField.getText(); //se obtiene el numero del empleado para filtrar en la busqueda del emplado.
				contEmp = 0;

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					if (numEmp.length() == 0) { //Se obtiene la longitud de la variable numEmp para validar si el campo no esta en blanco.
						limpiar();
						inicializarvar();
						JOptionPane.showMessageDialog(null,
								"Ingresar numero de empleado"); //Mensaje que se le muestra al usuario para que ingrese el numero del empleado.
					} else {
						obtenerInfEmp(numEmp); //Si la variable no esta vacia
					}

					contAce++; // Si ya se busco un empleado y se desea buscar
								// otro sin cerrar la pantalla el contAce va a
								// ir aumentando y se va a mandar llamar el
								// metodo de limpiar.

					if (contAce > 1) {
						limpiar(); //Metodo para limpiar la pantalla.
					}

					if (numEmp.length() != 0) {
						if (contEmp == 0) {
							JOptionPane.showMessageDialog(null,
									"Empleado no registrado");
							chckbxCubriTurno.setEnabled(false);
							inicializarvar();
							limpiar();
						}
					}

					textField_1.setText(nombre); //En el campo nombre del formulario se pone el nombre que se  
												//obtubo de la consulta correspondiente al numero del empleado.
					
				
					if (chofer == 1) {
						textField_2.setText("Chofer"); // Se pone chofer en el campo rol.
						chckbxCubriTurno.setEnabled(false); // se inabilita el checkbox cubrio turno.
					}
					if (cargador == 1) {
						textField_2.setText("Cargador");  // Se pone Cargador en el campo rol.
						chckbxCubriTurno.setEnabled(false);   //se inabilita el checkbox cubrio turno.
					}
					if (auxiliar == 1) {
						textField_2.setText("Auxiliar");   // Se pone Auxiliar en el campo rol.
						chckbxCubriTurno.setEnabled(true);  //se habilita el checkbox cubrio turno unicamente cuando el rol del empleado sea auxiliar.
					}
					if (tipoInt == 1) {
						textField_4.setText("Interno"); // Se pone Interno en el campo Tipo.
					} 
					if (tipoExt == 1) {
						textField_4.setText("Externo");  //Se pone Externo en el campo Tipo.
					}
				}
			}
		});
		textField.setBounds(427, 84, 114, 29);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setBounds(113, 133, 219, 29);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEnabled(false);
		textField_2.setBounds(112, 173, 132, 29);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setToolTipText("Fecha");
		textField_3.setBounds(112, 213, 132, 29);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		obtenerFecha();

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTipo.setBounds(321, 161, 30, 50);
		contentPane.add(lblTipo);

		textField_4 = new JTextField();
		textField_4.setEnabled(false);
		textField_4.setBounds(364, 173, 114, 29);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		chckbxCantidadDeEntregas = new JCheckBox("Cantidad de entregas");
		chckbxCantidadDeEntregas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//evento para cuando seleccione el checkbox cantidad de entregas se habilita o desabilita el spinner para poner el numero de las cantidades de entregas.
				if (chckbxCantidadDeEntregas.isSelected() == (true)) {
					spinner.setEnabled(true);
				} else {
					spinner.setEnabled(false);
					spinner.setValue(0); //Pone el valor de 0 en el spinner cuando el combo se desactiva.

				}
			}
		});
		chckbxCantidadDeEntregas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCantidadDeEntregas.setBounds(52, 258, 150, 23);
		contentPane.add(chckbxCantidadDeEntregas);

		chckbxCubriTurno = new JCheckBox("Cubri\u00F3 turno");
		chckbxCubriTurno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Condicion para que se muestre el combobox de cubrio turno si se selecciona o deselecciona el checkbox de cubrio turno.
				if (chckbxCubriTurno.isSelected() == (true)) {
					comboBox.setVisible(true);
				} else {
					comboBox.setVisible(false);
					comboBox.setSelectedItem(0);
				}
			}
		});
		chckbxCubriTurno.setEnabled(false);
		chckbxCubriTurno.setFont(new Font("Tahoma", Font.PLAIN, 13));
		chckbxCubriTurno.setBounds(254, 258, 97, 23);
		contentPane.add(chckbxCubriTurno);

		spinner = new JSpinner();
		spinner.setEnabled(false);
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 13));
		spinner.setBounds(77, 281, 114, 29);
		contentPane.add(spinner);

		btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String numEmp,fecha,turnoCubierto;
				int cubrioTurno,cantidadEnt;

				contFech = 0;  //Iniciliza la variable
				numEmp = textField.getText();  //Almacena el numero del empleado que se ingreso en la variable numEmp.
				fecha = textField_3.getText();  //Almacena la fecha en la variable fecha.

				if (numEmp.length() == 0) {  //Si la variable numEmp esta vacia muestra un mensaje al usuario para que ingrese el numero del empleado.
					limpiar();
					inicializarvar();
					JOptionPane.showMessageDialog(null,
							"Ingresar Numero de empleado");
				} else {
					consFecha(numEmp, fecha); //Si la variable es diferente de vacia, manda llamar el siguiente metodo y le envia como parametro el numero del empleado y la fecha.
				}

				if (chckbxCubriTurno.isSelected() == (true)) { // almacena 1 en una variable si el checkbox de cubrio turno esta seleccionado, de lo contrario almacena el 0.
					cubrioTurno = 1;
				} else {
					cubrioTurno = 0;
				}

				turnoCubierto =(String) comboBox.getSelectedItem();  //Obtiene el valor de la posicion del combobox que esta seleccionado.
				cantidadEnt = (int) spinner.getValue();              //Obtiene la cantidad de entregas que se tecleen en el spinner.

				if (chckbxCubriTurno.isSelected() == (false)) {
					turnoCubierto = "";
				}

				if (numEmp.length() != 0) {   //Si numEmp es diferente de vacio entra porque si se ingreso un numero de empleado para buscar.
					if (cantidadEnt == 0) {  // Si la variable viene vacia muestra un mensaje para que ingrese la cantidad de entregas.
						JOptionPane.showMessageDialog(null,
								"Ingresar cantidad de entregas");
					} else if (numEmp.length() == 0) {  //Si la variable viene vacia muestra un mensaje para que ingrese el numero del empleado.
						JOptionPane.showMessageDialog(null,
								"Ingresar el numero de empleado");
					} else if (fecha.length() == 0) {
						JOptionPane.showMessageDialog(null, "Ingresar fecha");
					} else if (contFech >= 1) {  //Si contFech es mayor o igual a 1 quiere decir que el empleado ya tiene registrado un movimiento en esa fecha.
						JOptionPane.showMessageDialog(null, "El empleado "
								+ numEmp
								+ " ya tiene registrada entregas en la fecha "
								+ fecha);
					} else if (fecha.compareTo(fechasistema) > 0) {   //compara la fecha que se ingreso con la fecha actual.
						JOptionPane
								.showMessageDialog(null,
										"La fecha a ingresar no debe ser mayor al dia actual");
						textField_3.setText(fechasistema);   // Pone la fecha actual en el campo.
					} else if (nombre.length() == 0) {
						JOptionPane.showMessageDialog(null,
								"Faltan los datos del empleado");  //Cuando se quiere aguardar un registro poniendo solo el numero del empleado sin darle enter para que lo busque.
					} else {
						insertarMov(numEmp, fecha, cubrioTurno, turnoCubierto,  //Llama metodo para insertar el movimiento al empleado.
								cantidadEnt);
						limpiar();  //ya que hace el insert limpia el formulario.
					}

				}

			}
		});
		btnOk.setBounds(364, 326, 89, 23);
		contentPane.add(btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(469, 326, 89, 23);
		contentPane.add(btnCancel);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Chofer",
				"Cargador" }));
		comboBox.setBounds(264, 281, 110, 28);
		contentPane.add(comboBox);
		comboBox.setVisible(false);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 118, 531, 201);
		contentPane.add(scrollPane_1);
	}

	public void consFecha(String numEmp, String fecha) {
		//Metodo para saber si ese numero de empleado tiene movimientos de entregas en la fecha que se ingreso.
		MyDataAcces conexion = new MyDataAcces();
		ResultSet resultado;
		try {
			resultado = conexion
					.getQuery("select fecha from capmovimientos where fecha="
							+ "\"" + fecha + "\"" + " and numeroEmp=" + numEmp
							+ "");

			while (resultado.next()) {

				contFech++; //Variable que incrementa si se encontraron movimientos en esa fecha.
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar los movimientos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void obtenerInfEmp(String numemp) {
		//Metodo que obtiene la informacion del numero del empleado que esta recibiendo como parametro.
		MyDataAcces conexion = new MyDataAcces();  //Instancia la clase donde se encuentra la conexion para la BD.
		ResultSet resultado;  //Variable para obtener los resultados de la consulta.
		try {
			resultado = conexion
					.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="
							+ numemp + "");

			while (resultado.next()) {
				
				//Almacena los valores que trajo la consulta en las variables.
				nombre = resultado.getString("Nombre");
				chofer = resultado.getInt("Chofer");
				cargador = resultado.getInt("Cargador");
				auxiliar = resultado.getInt("Auxiliar");
				tipoInt = resultado.getInt("tipoInt");
				tipoExt = resultado.getInt("tipoExt");

				contEmp++; //Variable para saber si existe el empleado que se desea buscar.
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Error : Al consultar el empleado", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void insertarMov(String numEmp, String fecha, int cubrioTurno,
			String turnoCubierto, int cantidadEnt) {
		//Metodo para hacer el insert del movimiento con los valores recibidos como parametros.
		MyDataAcces conexion = new MyDataAcces();
		try {
			conexion.setQuery("INSERT INTO `capmovimientos`(`numeroEmp`, `fecha`,`cubrioTurno`,`turnoCubierto`,`cantidadEntregas`) VALUES ('"
					+ numEmp
					+ "','"
					+ fecha
					+ "','"
					+ cubrioTurno
					+ "','"
					+ turnoCubierto + "','" + cantidadEnt + "')");
			JOptionPane.showMessageDialog(null,
					"Los datos se guardaron correctamente"); //Muestra mensaje confirmando que los datos se guardaron correctamente.
			textField_3.setText(fechasistema); //Pone la fecha actual en el campo fecha.
			textField.setText("");  //Limpia el textfield del numero del empleado.
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Error : Al insertar los datos", "Mensaje",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void inicializarvar() {
		//Metodo para inicializar las variables.
		nombre = "";
		chofer = 0;
		cargador = 0;
		auxiliar = 0;
		tipoInt = 0;
		tipoExt = 0;
		contFech = 0;
	}

	public void limpiar() {
		//Metodo para limpiar el formulario.
		textField_1.setText("");
		textField_2.setText("");
		textField_4.setText("");
		chckbxCubriTurno.setSelected(false);
		comboBox.setVisible(false);
		chckbxCantidadDeEntregas.setSelected(false);
		spinner.setEnabled(false);
		spinner.setValue(0);
		textField_3.setText(fechasistema);

	}

	public void obtenerFecha() {
		//Obtiene la fecha del dia actual y la pone en el campo fecha del formulario.
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
		formateador.format(ahora);
		fechasistema = formateador.format(ahora);
		textField_3.setText(formateador.format(ahora));
	}

}
